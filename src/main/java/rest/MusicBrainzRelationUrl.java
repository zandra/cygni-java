package rest;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MusicBrainzRelationUrl {

	@JsonProperty("resource")
	private String resource;
	
	public MusicBrainzRelationUrl() { }
		
	public String getResource() {
		return resource;
	}
}
