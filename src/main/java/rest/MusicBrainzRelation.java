package rest;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MusicBrainzRelation {

	@JsonProperty("url")
	private MusicBrainzRelationUrl url;

	@JsonProperty("type")
	private String type;
	
	public MusicBrainzRelation() { }
		
	public String getType() {
		return type;
	}
	
	public MusicBrainzRelationUrl getUrl() {
		return url;
	}
}
