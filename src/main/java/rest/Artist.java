package rest;

import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

public class Artist {

	public static final String MUSIC_BRAINZ_URL_TEMPLATE =
			"https://musicbrainz.org/ws/2/artist/%s/?fmt=json&inc=url-rels+release-groups";
	private static final String WIKI_TEMPLETE =
			"https://en.wikipedia.org/w/api.php?action=query&format=json&prop=extracts&exintro=true&redirects=true&titles=%s";
	private static final String COVER_ARTS_TEMPLATE =
			"http://coverartarchive.org/release-group/%s";
	
	private final String mbid;
    private String description;
    private List<ReleaseGroup> albums;
    
    public Artist(String mbid) throws JSONException {
    	this.mbid = mbid;
    	RestTemplate restTemplate = new RestTemplate();
    	
    	String mbUrl = String.format(MUSIC_BRAINZ_URL_TEMPLATE, mbid);
    	MusicBrainzInfo mbInfo = restTemplate.getForObject(mbUrl, MusicBrainzInfo.class);

    	setDescription(restTemplate, mbInfo);
    	setAlbums(restTemplate, mbInfo);
    }
    
    public String getMbid() {
        return mbid;
    }
    
    public String getDescription() {
    	return description;
    }
    
    public List<ReleaseGroup> getAlbums() {
    	return albums;
    }
    
    private void setDescription(RestTemplate restTemplate, MusicBrainzInfo mbInfo) throws JSONException {
    	String urlName = mbInfo.getWikiUrl().substring("https://en.wikipedia.org/wiki/".length());
		String wikiUrl = String.format(WIKI_TEMPLETE, urlName);

    	String wikiString = restTemplate.getForObject(wikiUrl, String.class);
    	JSONObject wikiObject = new JSONObject(wikiString).getJSONObject("query").getJSONObject("pages");
    	JSONArray array = wikiObject.names();
    	JSONObject p = wikiObject.getJSONObject(array.getString(0));
    	this.description = p.getString("extract");
    }

    private void setAlbums(RestTemplate restTemplate, MusicBrainzInfo mbInfo) {
    	albums = mbInfo.getAlbums();
    	albums.parallelStream().forEach((album) -> {
       		String coverArtUrl = String.format(COVER_ARTS_TEMPLATE, album.getId());
    		try {
    			String albumString = restTemplate.getForObject(coverArtUrl, String.class);
    			JSONObject coverObject = new JSONObject(albumString);
    			JSONArray images = coverObject.getJSONArray("images");
    			for(int i = 0; i < images.length(); i++) {
    				if(images.getJSONObject(i).getBoolean("front")) {
    					String image = images.getJSONObject(i).getString("image");
    					album.setImage(image);
    					break;
    				}
    			}
    		} catch (HttpClientErrorException e) {
    			if(!e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
    				throw(e);
    			}
    		} catch (JSONException e) {
				e.printStackTrace();
			}
        });
    }
}
