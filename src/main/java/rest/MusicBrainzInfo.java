package rest;

import java.util.LinkedList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MusicBrainzInfo {

	private String wikiUrl;
	@JsonProperty("release-groups")
	private List<ReleaseGroup> albums;
	
	public MusicBrainzInfo() {
		albums = new LinkedList<ReleaseGroup>();
	}
	
	public void setRelations(List<MusicBrainzRelation> relations) {
		for(MusicBrainzRelation r : relations) {
			if(r.getType().equals("wikipedia")) {
				this.wikiUrl = r.getUrl().getResource();
			}
		}
	}
		
	public List<ReleaseGroup> getAlbums() {
		return albums;
	}
	
	public String getWikiUrl() {
		return wikiUrl;
	}
}
