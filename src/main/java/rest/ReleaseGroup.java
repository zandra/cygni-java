package rest;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ReleaseGroup {
	
	@JsonProperty("title")
	private String title;
	@JsonProperty("id")
	private String id;
	private String image;
	
	public ReleaseGroup() { }
		
	public String getTitle() {
		return title;
	}
	
	public String getId() {
		return id;
	}
	
	public void setImage(String image) {
		this.image = image;
	}
	
	public String getImage() {
		return image;
	}
}
